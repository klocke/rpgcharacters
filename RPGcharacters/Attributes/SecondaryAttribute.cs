﻿using System.Text;

namespace RPGcharacters.Characters
{
    /// <summary>
    /// Secondary Attribute: Three properties which are calculated from a Primary Attrirbute through some logic.
    /// </summary>
    public class SecondaryAttribute : Attribute
    {

        /// <summary>
        /// Constructor from a PrimaryAttribute
        /// </summary>
        /// <param name="totalAttribute"></param>
        public SecondaryAttribute(PrimaryAttribute totalAttribute)
        {
            Health = 10 * totalAttribute.Vitality;
            ArmorRating = totalAttribute.Strength + totalAttribute.Dexterity;
            ElementalResistance = totalAttribute.Intelligence;
        }

        /// <summary>
        /// Parameterless constructor. 
        /// </summary>
        public SecondaryAttribute() { }

        public double Health { get; set; }
        public double ArmorRating { get; set; }
        public double ElementalResistance { get; set; }

        /// <summary>
        /// Overrides Equals in base class. As with Primaryattributes, SecondaryAttributes are considered
        /// equal only if all attribute properties are equal.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return obj is SecondaryAttribute other && Health == other.Health && ArmorRating == other.ArmorRating && ElementalResistance == other.ElementalResistance;
        }

        /// <summary>
        /// Override of ToString. Represents the Attribute as a string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sb = new();
            sb.AppendLine($"Health: {Health}");
            sb.AppendLine($"Armor Rating: {ArmorRating}");
            sb.AppendLine($"Elemental Resistance: {ElementalResistance}");
            return sb.ToString();
        }
    }
}
