﻿using System.Text;

namespace RPGcharacters.Characters
{
    
    /// <summary>
    /// Primary attribute: Contains four stats for character,
    /// including an override of the + -operator and the abstract base class methods. 
    /// </summary>
    public class PrimaryAttribute : Attribute
    {

        public double Strength { get; set; }
        public double Dexterity { get; set; }
        public double Intelligence { get; set; }
        public double Vitality { get; set; }

        /// <summary>
        /// Constructor with default arguments set to 0.
        /// </summary>
        /// <param name="strength"></param>
        /// <param name="dexterity"></param>
        /// <param name="intelligence"></param>
        /// <param name="vitality"></param>
        public PrimaryAttribute(double strength = 0, double dexterity = 0, double intelligence = 0, double vitality = 0)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
            Vitality = vitality;
        }

        /// <summary>
        /// Overload of the + -operator.
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns></returns>
        public static PrimaryAttribute operator +(PrimaryAttribute lhs, PrimaryAttribute rhs)
        {
            return new PrimaryAttribute
            {
                Vitality = lhs.Vitality + rhs.Vitality,
                Strength = lhs.Strength + rhs.Strength,
                Dexterity = lhs.Dexterity + rhs.Dexterity,
                Intelligence = lhs.Intelligence + rhs.Intelligence
            };
        }

        /// <summary>
        /// Implementation of Equals().
        /// Two PrimaryAttribute instances are considered equal if all attributes are equal.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>
        /// true if equal, else false.
        /// </returns>
        public override bool Equals(object obj)
        {
            return obj is PrimaryAttribute other &&
                Vitality == other.Vitality &&
                Strength == other.Strength &&
                Dexterity == other.Dexterity &&
                Intelligence == other.Intelligence;
        }

        /// <summary>
        /// ToString implementation. Vitality is not shown.
        /// </summary>
        /// <returns>
        /// String representation
        /// </returns>
        public override string ToString()
        {
            StringBuilder sb = new();
            sb.AppendLine($"Strength: {Strength}");
            sb.AppendLine($"Dexterity: {Dexterity}");
            sb.AppendLine($"Intelligence: {Intelligence}");
            return sb.ToString();
        }
    }
}
