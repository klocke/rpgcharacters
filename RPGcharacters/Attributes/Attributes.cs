﻿namespace RPGcharacters.Characters
{
    /// <summary>
    /// Abstract Attribute class
    /// </summary>
    public abstract class Attribute
    {
        /// <summary
        /// Overrides Equals such that comparisons of Attributes is easy. Also makes it easier to write tests.
        /// </summary>
        /// <param name="obj">
        /// Other object ; the object we want to compare with.
        /// </param>
        /// <returns>
        /// true / false
        /// </returns>
        public abstract override bool Equals(object obj);

        /// <summary>
        /// Overrides ToString for representation in the console. 
        /// </summary>
        /// <returns>
        /// String representation of Attributes
        /// </returns>
        public abstract override string ToString();
    }

}
