﻿using RPGcharacters.Characters;
using RPGcharacters.Equipment;
using System;

namespace RPGcharacters
{
    class Program
    {
        static void Main(string[] args)
        {
            Character alice = new Warrior("Alice");
            Console.WriteLine(alice);
            Character bob = new Mage("Bob");
            Console.WriteLine(bob);

            Console.WriteLine("Press any key to continue. ");
            var _ = Console.ReadLine();

            Console.WriteLine("\nBoth Alice and Bob wants to wear a cloth on their head for _some_ reason... ");
            Armor cloth = new()
            {
                Name = "Common cloth head armor",
                RequiredLevelForEquipment = 1,
                ItemSlot = Slot.Head,
                ArmorType = ArmorTypes.Cloth,
                ArmorAttribute = new PrimaryAttribute() { Vitality = 1, Intelligence = 5 }
            };

            Console.WriteLine(bob.Equip(cloth));
            Console.WriteLine("Bob managed to put on a piece of cloth.");

            Console.WriteLine("Press any key to continue. ");
            _ = Console.ReadLine();


            try
            {
                alice.Equip(cloth);
            }
            catch (InvalidArmorException e)
            {
                Console.WriteLine("Oh no, Alice got lost in the cloth and had to tear it off!\n");
                Console.WriteLine(e.Message);
            };

            Console.WriteLine("Press any key to continue. ");
            _ = Console.ReadLine();

            Console.WriteLine("Both Alice and Bob tries to pick up an axe:");
            Weapon axe = new()
            {
                Name = "Common axe",
                RequiredLevelForEquipment = 1,
                ItemSlot = Slot.Weapon,
                WeaponType = WeaponTypes.Axe,
                WeaponAttribute = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };

            alice.Equip(axe);
            Console.WriteLine("Alice equipped an axe.");

            Console.WriteLine("Press any key to continue. ");
            _ = Console.ReadLine();


            try
            {
                bob.Equip(axe);
            }
            catch (InvalidWeaponException e)
            {
                Console.WriteLine("Oh no, the grip was too slippery for Bob!");
                Console.WriteLine(e.Message);
            }

            Console.WriteLine("Press any key to continue. ");
            _ = Console.ReadLine();

            Console.WriteLine("Both of them gained 5 levels in the process of equipping items.... ");

            alice.LevelUp(5);
            bob.LevelUp(5);

            Console.WriteLine("Press any key to continue. ");
            _ = Console.ReadLine();

            Console.WriteLine("New stats for Alice and Bob:");


            Console.WriteLine(alice);
            Console.WriteLine(bob);



        }
    }
}
