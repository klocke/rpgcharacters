﻿using System;

namespace RPGcharacters.Equipment
{

    /// <summary>
    /// Custom WeaponException. Inherts from Exception class with a default message.
    /// </summary>
    [System.Serializable]
    public class InvalidWeaponException : InvalidItemException
    {
        private const string message = "Invalid Weapon!";
        public InvalidWeaponException(string message = message) : base(message) { }

    }


    /// <summary>
    /// Weapon enumerator
    /// </summary>
    public enum WeaponTypes
    {
        Axe,
        Bow,
        Dagger,
        Hammer,
        Staff,
        Sword,
        Wand
    }

    /// <summary>
    /// Weapon attributes
    /// </summary>
    public struct WeaponAttributes
    {
        public double Damage;
        public double AttackSpeed;
    }

    /// <summary>
    /// Weapon subclass of Item.
    /// </summary>
    public class Weapon : Item
    {
        
        /// <summary>
        /// WeaponType (enumerator)
        /// </summary>
        public WeaponTypes WeaponType { get; set; }


        /// <summary>
        /// Weapon attributes.
        /// </summary>
        public WeaponAttributes WeaponAttribute { get; set; }

        /// <summary>
        /// Damage per second from weapon. 
        /// </summary>
        public double WeaponDps { get => WeaponAttribute.Damage * WeaponAttribute.AttackSpeed; }


        /// <summary>
        /// Constructor for weapon. 
        /// </summary>
        /// <param name="weaponType"></param>
        /// <param name="weaponAttributes"></param>
        /// <param name="name"></param>
        /// <param name="requiredLevel"></param>
        /// <param name="itemSlot"></param>
        public Weapon(WeaponTypes weaponType, WeaponAttributes weaponAttributes, string name, int requiredLevel, Slot itemSlot) : base(name, requiredLevel, itemSlot)
        {
            this.WeaponType = weaponType;
            this.WeaponAttribute = weaponAttributes;
        }
        public Weapon() : base() { }
    }

}
