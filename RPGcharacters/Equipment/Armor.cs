﻿using RPGcharacters.Characters;
using System;

namespace RPGcharacters.Equipment
{
    /// <summary>
    /// Enumerator for armortypes
    /// </summary>
    public enum ArmorTypes
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }

    /// <summary>
    /// Custom InvalidArmorException. 
    /// </summary>
    [System.Serializable]
    public class InvalidArmorException : InvalidItemException
    {
        private const string message = "Invalid Armor!";
        public InvalidArmorException(string message = message) : base(message) { }

    }


    /// <summary>
    /// Armor subclsas of item
    /// </summary>
    public class Armor : Item
    {
        /// <summary>
        /// Each armor is of a special type
        /// </summary>
        public ArmorTypes ArmorType { get; set; }

        /// <summary>
        /// Armor has some attributes
        /// </summary>
        public PrimaryAttribute ArmorAttribute { get; set; }

        /// <summary>
        /// Empty constructor
        /// </summary>
        public Armor() : base() { }

        /// <summary>
        /// Constructor with parameters
        /// </summary>
        /// <param name="armorattribute"></param>
        /// <param name="name"></param>
        /// <param name="requiredLevel"></param>
        /// <param name="itemSlot"></param>
        public Armor(PrimaryAttribute armorattribute, string name, int requiredLevel, Slot itemSlot) : base(name, requiredLevel, itemSlot)
        {
            this.ArmorAttribute = armorattribute;
        }
    }
}
