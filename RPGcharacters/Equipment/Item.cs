﻿using System;
namespace RPGcharacters.Equipment
{
    [System.Serializable]
    public  abstract class InvalidItemException : Exception
    {
        
        public InvalidItemException(string message) : base(message) { }

    }


    /// <summary>
    /// Slot enumerator. Available item placements
    /// </summary>
    public enum Slot
    {
        Head,
        Body,
        Legs,
        Weapon
    }

    /// <summary>
    /// Item interface
    /// </summary>
    interface IItem
    {
        public string Name { get; set; }
        public int RequiredLevelForEquipment { get; set; }
        public Slot ItemSlot { get; set; }
    }



    /// <summary>
    /// Item class containing logic for general Items. Abstract class -> all items must be of a special kind and inherits the logic of an item. 
    /// </summary>
    public abstract class Item :IItem
    {

        /// <summary>
        /// Item name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Items should require the item user to be of a minimum level
        /// </summary>
        public int RequiredLevelForEquipment { get; set; } = 1;


        /// <summary>
        /// Slot where the item is stored on a user. 
        /// </summary>
        public Slot ItemSlot { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public Item() { }

        /// <summary>
        /// Constructor with parameters.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="requiredLevel"></param>
        /// <param name="itemSlot"></param>
        public Item(string name, int requiredLevel, Slot itemSlot)
        {
            Name = name;
            RequiredLevelForEquipment = requiredLevel;
            ItemSlot = itemSlot;
        }
    }
}
