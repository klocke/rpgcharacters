﻿using RPGcharacters.Equipment;

namespace RPGcharacters.Characters
{
    public class Warrior : Character
    {

        /// <summary>
        /// static starting attributes. 
        /// </summary>
        public static readonly PrimaryAttribute _basePrimary = new()
        {
            Vitality = 10,
            Strength = 5,
            Dexterity = 2,
            Intelligence = 1
        };

        /// <summary>
        /// static attributes for level increment
        /// </summary>
        public static readonly PrimaryAttribute _levelUpAttribute = new()
        {
            Vitality = 5,
            Strength = 3,
            Dexterity = 2,
            Intelligence = 1
        };

        /// <summary>
        /// Valid weapon types for subclass
        /// </summary>
        private static readonly WeaponTypes[] _validWeaponTypes = new[]
        {
             WeaponTypes.Axe, WeaponTypes.Hammer, WeaponTypes.Sword
        };

        /// <summary>
        /// Valid armor types for subclass
        /// </summary>
        private static readonly ArmorTypes[] _validArmorTypes = new[]
       {
              ArmorTypes.Plate, ArmorTypes.Mail
        };

        /// <summary>
        /// Primary Attribute. Strength for Warrior. 
        /// </summary>
        protected override double SpecialAttribute { get => TotalPrimaryAttribute.Strength; }

        /// <summary>
        /// Constructor. Calls base with static attrs.
        /// </summary>
        /// <param name="name"></param>
        public Warrior(string name) : base(name, _basePrimary, _levelUpAttribute)
        {
            ValidWeaponTypes = _validWeaponTypes;
            ValidArmorTypes = _validArmorTypes;
        }


    }
}
