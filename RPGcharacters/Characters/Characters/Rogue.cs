﻿using RPGcharacters.Equipment;

namespace RPGcharacters.Characters
{
    /// <summary>
    /// Rogue character
    /// </summary>
    public class Rogue : Character
    {
        /// <summary>
        /// static starting attributes. 
        /// </summary>
        public static readonly PrimaryAttribute _basePrimary = new()
        {
            Vitality = 8,
            Strength = 2,
            Dexterity = 6,
            Intelligence = 1
        };

        /// <summary>
        /// static attributes for level increment
        /// </summary>
        public static readonly PrimaryAttribute _levelUpAttribute = new()
        {
            Vitality = 3,
            Strength = 1,
            Dexterity = 4,
            Intelligence = 1
        };

        /// <summary>
        /// Valid weapon types for subclass
        /// </summary>
        private static readonly WeaponTypes[] _validWeaponTypes = new[]
        {
            WeaponTypes.Dagger, WeaponTypes.Sword
        };

        /// <summary>
        /// Valid armor types for subclass
        /// </summary>
        private static readonly ArmorTypes[] _validArmorTypes = new[]
       {
             ArmorTypes.Leather, ArmorTypes.Mail
        };

        /// <summary>
        /// Primary attribute. Dexterity for rogue. 
        /// </summary>
        protected override double SpecialAttribute { get => TotalPrimaryAttribute.Dexterity; }
        
        /// <summary>
        /// Constructor. Calls base with static attrs.
        /// </summary>
        /// <param name="name"></param>
        public Rogue(string name) : base(name, _basePrimary, _levelUpAttribute)
        {
            ValidWeaponTypes = _validWeaponTypes;
            ValidArmorTypes = _validArmorTypes;
        }
    }
}