﻿using RPGcharacters.Equipment;

namespace RPGcharacters.Characters
{
    /// <summary>
    /// Ranger subclass of character
    /// </summary>
    public class Ranger : Character
    {
        /// <summary>
        /// Starting _base primary for ranger class.
        /// </summary>
        public static readonly PrimaryAttribute _basePrimary = new()
        {
            Vitality = 8,
            Strength = 1,
            Dexterity = 7,
            Intelligence = 1
        };

        /// <summary>
        /// Level increment attribute for ranger class. 
        /// </summary>
        public static readonly PrimaryAttribute _levelUpAttribute = new()
        {
            Vitality = 2,
            Strength = 1,
            Dexterity = 5,
            Intelligence = 1
        };



        /// <summary>
        /// Valid weapontypes for ranger class
        /// </summary>
        private static readonly WeaponTypes[] _validWeaponTypes = new[]
        {
            WeaponTypes.Bow
        };


        /// <summary>
        /// Valid armor types for ranger class
        /// </summary>
        private static readonly ArmorTypes[] _validArmorTypes = new[]
        {
            ArmorTypes.Leather, 
            ArmorTypes.Mail
        };

        /// <summary>
        /// Primary attribute for ranger is dexterity.
        /// </summary>
        protected override double SpecialAttribute { get => TotalPrimaryAttribute.Dexterity; }

        /// <summary>
        /// Constructor. Calls base with static properties. 
        /// </summary>
        /// <param name="name"></param>
        public Ranger(string name) : base(name, _basePrimary, _levelUpAttribute)
        {
            ValidWeaponTypes = _validWeaponTypes;
            ValidArmorTypes = _validArmorTypes;
        }


    }
}
