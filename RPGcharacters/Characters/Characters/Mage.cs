﻿using RPGcharacters.Equipment;

namespace RPGcharacters.Characters
{
    /// <summary>
    /// Mage subclass of Character
    /// </summary>
    public class Mage : Character
    {
        /// <summary>
        /// Starting _base primary for Mage class.
        /// </summary>
        private static readonly PrimaryAttribute _basePrimary = new()
        {
            Vitality = 5,
            Strength = 1,
            Dexterity = 1,
            Intelligence = 8
        };

        /// <summary>
        /// Level increment for Mage class. 
        /// </summary>
        private static readonly PrimaryAttribute _levelUpAttribute = new()
        {
            Vitality = 3,
            Strength = 1,
            Dexterity = 1,
            Intelligence = 5
        };

        /// <summary>
        /// Valid weapontypes for Mage class
        /// </summary>
        private static readonly WeaponTypes[] _validWeaponTypes = new[] 
        {
            WeaponTypes.Staff, 
            WeaponTypes.Wand 
        };

        /// <summary>
        /// Valid armor types for Mage class
        /// </summary>
        private static readonly ArmorTypes[] _validArmorTypes = new[] 
        {
            ArmorTypes.Cloth 
        };

        /// <summary>
        /// Primary attribute for mage class is intelligence
        /// </summary>
        protected override double SpecialAttribute { get => TotalPrimaryAttribute.Intelligence; }


        /// <summary>
        /// Constructor. Only takes a name.
        /// </summary>
        /// <param name="name"></param>
        public Mage(string name) : base(name, _basePrimary, _levelUpAttribute)
        {
            ValidWeaponTypes = _validWeaponTypes;
            ValidArmorTypes = _validArmorTypes;
        }


    }
}