﻿using RPGcharacters.Equipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RPGcharacters.Characters
{
    /// <summary>
    /// Interface for character class. 
    /// Requires all inherited classes to implement
    ///         * Equip() : All characters must handle equipment
    ///         * LevelUp() : All characters can level up.
    /// </summary>
    interface ICharacter
    {
        public string Equip(Weapon weapon);
        public string Equip(Armor armor);
        public void LevelUp();

    }


    /// <summary>
    /// Character class. 
    /// Abstract -- No character can be a general "character" type, but all characters share much functionality.
    /// </summary>
    public abstract class Character : ICharacter
    {

        /// <summary>
        /// All characters have a name name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// All 
        /// </summary>
        public int Level { get; set; } = 1;

        /// <summary>
        /// Equipment dictionary; Keeps track of the equipment on a given character.
        /// </summary>
        private Dictionary<Slot, Item> Equipment = new();

        /// <summary>
        /// Valid Weapon types for a character
        /// </summary>
        public WeaponTypes[] ValidWeaponTypes { get; set; }
       
        /// <summary>
        /// Valid Armor types for a character
        /// </summary>
        public ArmorTypes[] ValidArmorTypes { get; set; }

        /// <summary>
        /// Speciality: Special for each type of character, hence abstract. 
        /// numerical valus of PrimaryAttribute (usually the largest)
        /// </summary>
        protected abstract double SpecialAttribute{ get; }


        /// <summary>
        /// Base attributes ( character stats without considering equipment)
        /// </summary>
        public PrimaryAttribute BasePrimaryAttribute { get; set; }

        /// <summary>
        /// Total Attributes: Includes armor and specials logic.
        /// </summary>
        protected PrimaryAttribute TotalPrimaryAttribute => GetTotalAttributes();

        /// <summary>
        /// Method for updating attributes after leveling up, or adding armor / weapon.
        /// Updates Secondaryattributes. 
        /// </summary>
        public void UpdateAttributes()
        {
            SecondaryAttribute = new SecondaryAttribute(TotalPrimaryAttribute);
        }

        /// <summary>
        /// character + armor attributes are calculated and returned here. 
        /// </summary>
        /// <returns></returns>
        protected PrimaryAttribute GetTotalAttributes()
        {
            PrimaryAttribute armorAttributes = new();
            foreach (var item in Equipment)
            {
                if (item.Value is Armor armor)
                {
                    armorAttributes += armor.ArmorAttribute;
                }

            }

             return BasePrimaryAttribute + armorAttributes;
        }


        /// <summary>
        /// constant stats added for each level. Setter is only possble in init. 
        /// </summary>
        private  PrimaryAttribute LevelUpPrimaryAttribute { get; init; }

        /// <summary>
        /// Secondary attributes
        /// </summary>
        public SecondaryAttribute SecondaryAttribute { get; set; }

        /// <summary>
        /// Method for fetching the damage per second from a character. s
        /// </summary>
        public double CharacterDps => GetCharacterDps();

        /// <summary>
        /// Possibility for implementing damage-dealing
        /// </summary>
        /// <param name="other"></param>
        public void DealDamage(Character other)
        {
            throw new NotImplementedException();

        }

        /// <summary>
        /// Weapon equipment which throws a custom exception if the weapon is not valid. 
        /// </summary>
        /// <param name="weapon"></param>
        /// <returns>
        /// Sucess message
        /// </returns>
        public virtual string Equip(Weapon weapon)
        {
            
            /* Checks if null -> Exception 
             * Checks if weapon is of a type valid for character
             * Checks if itemslot is weapon ( not necessary according to task, but such a small thing to add)
             * Checks if character level is above or equal to required level.
             * Updates attributes after succesfull equipment. 
             */
            weapon = weapon ?? throw new InvalidWeaponException(); 


            
            if (ValidWeaponTypes.Contains(weapon.WeaponType) &&
                weapon.ItemSlot == Slot.Weapon &&
                Level >= weapon.RequiredLevelForEquipment)
            {
                Equipment[weapon.ItemSlot] = weapon;
                UpdateAttributes();
                return "New weapon equipped!";
            }
            else
            {
                throw new InvalidWeaponException();
            }
        }

        /// <summary>
        /// Armor equipment which throws a custom exception if the armor is not valid
        /// </summary>
        /// <param name="armor"></param>
        /// <returns>
        /// Success message
        /// </returns>
        public virtual string Equip(Armor armor)
        {
            if (armor.RequiredLevelForEquipment > Level || !ValidArmorTypes.Contains(armor.ArmorType))
            {
                throw new InvalidArmorException();
            }
            else
            {
                Equipment[armor.ItemSlot] = armor;
                UpdateAttributes();
                return "New armor equipped!";
            }
        }

        /// <summary>
        /// Character constructor. Checks if name is valid, 
        /// and sets the inital attributes for the character
        /// </summary>
        /// <param name="name"></param>
        /// <param name="baseAttribute"></param>
        /// <param name="levelUpAttribute"></param>
        public Character(string name, PrimaryAttribute baseAttribute, PrimaryAttribute levelUpAttribute)
        {
            Name = !string.IsNullOrEmpty(name) ? name : throw new ArgumentNullException(nameof(name));
            BasePrimaryAttribute = baseAttribute;
            LevelUpPrimaryAttribute = levelUpAttribute;
            SecondaryAttribute = new SecondaryAttribute(BasePrimaryAttribute);
        }

        /// <summary>
        /// Method for levelling up once.
        /// </summary>
        public virtual void LevelUp()
        {
            Level += 1;
            BasePrimaryAttribute += LevelUpPrimaryAttribute;
            UpdateAttributes();
        }

        /// <summary>
        /// Method for levelling up multiple levels at once.
        /// </summary>
        /// <param name="nlevels"></param>
        public virtual void LevelUp(int nlevels)
        {
            if (nlevels <= 0)
            {
                throw new ArgumentException("Invalid level");
            }
            for (int i = 1; i <= nlevels; i++)
            {
                LevelUp();
            }
        }



        /// <summary>
        /// Logic for Computing the damage per second for a character.
        /// Iterates over 
        /// </summary>
        /// <returns></returns>
        private double GetCharacterDps()
        {
            double totalPrimaryAttribute = SpecialAttribute;
            double weaponDps;
            if (Equipment.ContainsKey(Slot.Weapon) && Equipment[Slot.Weapon] is Weapon weapon)
            {
                weaponDps = weapon.WeaponDps;

            }
            else
            {
                weaponDps = 1;
            }
            return weaponDps * (1 + totalPrimaryAttribute / 100);

        }

        /// <summary>
        /// String representation of character using StringBuilder.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {

            StringBuilder sb = new();
            sb.AppendLine("_________");
            sb.AppendLine($"{GetType().Name} {Name}");
            sb.AppendLine($"Level: {Level}\n");
            sb.AppendLine(TotalPrimaryAttribute.ToString());
            sb.AppendLine(SecondaryAttribute.ToString());
            sb.AppendLine($"DPS: {GetCharacterDps()}");
            sb.AppendLine("---------");

            return sb.ToString();
        }
    }

}
