﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using RPGcharacters.Equipment;
using RPGcharacters.Characters;


namespace RPGcharactersTest.EquipmentTests
{
    /// <summary>
    /// Testing for equipment. 
    /// </summary>
    public class EquipmentTest
    {


        /// <summary>
        /// Assert weapontype is set as expected. 
        /// </summary>
        [Fact]
        public void Constructor_WeaponType_ShouldBeSet()
        {
            WeaponTypes expected =  WeaponTypes.Axe;

            WeaponAttributes weaponAttributes = new() { Damage = 10, AttackSpeed = 0.7 };
            Weapon weapon = new(weaponType:expected, weaponAttributes:weaponAttributes, name:"Common axe", requiredLevel:1, itemSlot: Slot.Weapon);

            WeaponTypes actual = weapon.WeaponType;

            Assert.Equal(expected, actual);

        }

        /// <summary>
        /// Invalid weapon should throw exception
        /// </summary>
        [Fact]
        public void Equip_MageInvalidWeaponType_ShouldThrowCustomException()
        {

            // Arrange
            Mage mage = new("name");
            Weapon invalidMageWeapon = new            (
                weaponType: WeaponTypes.Axe,
                weaponAttributes: new WeaponAttributes { AttackSpeed = 0, Damage = 0 },
                name: "Axe",
                requiredLevel: 1,
                itemSlot: Slot.Weapon
                );

            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => mage.Equip(invalidMageWeapon));

        }

        /// <summary>
        /// Character should not be able to equip weapon before required level is reached. 
        /// </summary>
        [Fact]
        public void Equip_WeaponLevelRequirementTooHigh_ShouldThrowInvalidWeaponException()
        {
            Weapon testAxe = new()
            {
                Name = "Common axe",
                RequiredLevelForEquipment = 2, // higher level
                ItemSlot = Slot.Weapon,
                WeaponType = WeaponTypes.Axe,
                WeaponAttribute = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };

            Warrior warrior = new("Warrior Name");


            Assert.Throws<InvalidWeaponException>(() => warrior.Equip(testAxe));
        }

        /// <summary>
        /// Armor level requirement test.
        /// </summary>
        [Fact]
        public void Equip_ArmorLevelRequirementTooHigh_ShouldThrowInvalidArmorException()
        {
            Armor testPlateBody = new()
            {
                Name = "Common plate body armor",
                RequiredLevelForEquipment= 2,
                ItemSlot = Slot.Body,
                ArmorType = ArmorTypes.Plate,
                ArmorAttribute = new PrimaryAttribute() { Vitality = 2, Strength = 1 }
            };

            Warrior warrior = new("Warrior Name");

            Assert.Throws<InvalidArmorException>(() => warrior.Equip(testPlateBody));
        }


        /// <summary>
        /// Invalid weapon for warrior should throw.
        /// </summary>
        [Fact]
        public void Equip_InvalidWeaponType_ShouldThrowInvalidWeaponException()
        {
            Weapon testBow = new()
            {
                Name = "Common bow",
                RequiredLevelForEquipment = 1,
                ItemSlot = Slot.Weapon,
                WeaponType = WeaponTypes.Bow,
                WeaponAttribute = new WeaponAttributes() { Damage = 12, AttackSpeed = 0.8 }
            };

            Warrior warrior = new("Warrior Name");


            Assert.Throws<InvalidWeaponException>(() => warrior.Equip(testBow));
        }

        /// <summary>
        /// Invalid armor type test
        /// </summary>
        [Fact]
        public void Equip_InvalidArmorType_ShouldThrowInvalidArmorException()
        {
            Armor testClothHead = new()
            {
                Name = "Common cloth head armor",
                RequiredLevelForEquipment = 2,
                ItemSlot = Slot.Head,
                ArmorType = ArmorTypes.Cloth,
                ArmorAttribute = new PrimaryAttribute() { Vitality = 1, Intelligence = 5 }
            };

            Warrior warrior = new("Warrior Name");

            Assert.Throws<InvalidArmorException>(() => warrior.Equip(testClothHead));

        }

        /// <summary>
        /// Success message from valid weapon. 
        /// </summary>
        [Fact]
        public void Equip_ValidWeapon_ShouldReturnMessage()
        {
            string expected = "New weapon equipped!";
            Weapon testAxe = new()
            {
                Name = "Common axe",
                RequiredLevelForEquipment = 1, 
                ItemSlot = Slot.Weapon,
                WeaponType = WeaponTypes.Axe,
                WeaponAttribute = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };

            Warrior warrior = new("Warrior Name");

            string actual = warrior.Equip(testAxe);

            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Test returnmessage when valid armor is equipped. 
        /// </summary>
        [Fact]
        public void Equip_ValidArmor_ShouldReturnMessage()
        {
            string expected = "New armor equipped!";
            Armor testPlateBody = new()
            {
                Name = "Common plate body armor",
                RequiredLevelForEquipment = 1,
                ItemSlot = Slot.Body,
                ArmorType = ArmorTypes.Plate,
                ArmorAttribute = new PrimaryAttribute() { Vitality = 2, Strength = 1 }
            };

            Warrior warrior = new("Warrior Name");

            string actual = warrior.Equip(testPlateBody);

            Assert.Equal(expected, actual);
        }


        /// <summary>
        /// Calculation of DPS at construction is as expected
        /// </summary>
        [Fact]
        public void GetCharacterDps_NoWeaponEquipped_ShouldBeAsExpected()
        {
            double expected = 1 * (1 + ((double)5 / 100));
            Warrior warrior = new("Warrior Name");


            double actual = warrior.CharacterDps;

            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// DPS with armor is expected. 
        /// </summary>
        [Fact]
        public void GetCharacterDps_WithValidWeapon_ShouldBeAsExpected()
        {
            double expected = (7*1.1) * (1 + ((double)5 / 100));
            Warrior warrior = new("Warrior Name");
            Weapon testAxe = new()
            {
                Name = "Common axe",
                RequiredLevelForEquipment = 1,
                ItemSlot = Slot.Weapon,
                WeaponType = WeaponTypes.Axe,
                WeaponAttribute = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };

            warrior.Equip(testAxe);

            double actual = warrior.CharacterDps;

            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// Testing dps with both armor and weapon
        /// </summary>
        [Fact]
        public void GetCharacterDps_WithValidArmorAndWeapon_ShouldBeAsExpected()
        {
            double expected = (7 * 1.1) * (1 + ((double)(5+1) / 100));
            Warrior warrior = new("Warrior Name");
            Weapon testAxe = new()
            {
                Name = "Common axe",
                RequiredLevelForEquipment = 1,
                ItemSlot = Slot.Weapon,
                WeaponType = WeaponTypes.Axe,
                WeaponAttribute = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };

            Armor testPlateBody = new()
            {
                Name = "Common plate body armor",
                RequiredLevelForEquipment = 1,
                ItemSlot = Slot.Body,
                ArmorType = ArmorTypes.Plate,
                ArmorAttribute = new PrimaryAttribute() { Vitality = 2, Strength = 1 }
            };

            warrior.Equip(testAxe);
            warrior.Equip(testPlateBody);

            double actual = warrior.CharacterDps;

            Assert.Equal(expected, actual);
        }

        /// <summary>
        /// It should not be possible to equip null-items
        /// </summary>
        [Fact]
        public void Equip_NullItem_ShouldThrowException()
        {
            Weapon weapon = null;
            Warrior warrior = new("Warrior Name");


            Assert.Throws<InvalidWeaponException>(() => warrior.Equip(weapon));
        }
    }
}
