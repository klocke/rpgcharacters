﻿using Xunit;
using RPGcharacters.Equipment;
using RPGcharacters.Characters;
using System;

namespace RPGcharactersTest.CharactersTest
{

    public class CharacterTests
    {      

        /// <summary>
        /// Asserts name cannot be null
        /// </summary>
        [Fact]
        public void Constructor_NullName_ShouldThrowException()
        {
            // Arrange
            string name = null;

            // Act & Assert
            Assert.Throws<ArgumentNullException>(() => new Mage(name));


        }

        /// <summary>
        /// Asserts name cannot be emptystring
        /// </summary>
        [Fact]
        public void Constructor_EmptystringName_ShouldThrowException()
        {
            // Arrange
            string name = "";

            // Act & Assert
            Assert.Throws<ArgumentNullException>(() => new Mage(name));
        }

        /// <summary>
        /// Checks that name is set correctly
        /// </summary>
        [Fact]
        public void Constructor_ValidName_ShouldBeSet()
        {
            // Arrange
            string expectedName = "name";

            // Act

            Mage mage = new(expectedName);

            string actual = mage.Name;

            // Assert
            Assert.Equal(expectedName, actual);


        }
    }

    /// <summary>
    /// Testing for Mage class
    /// </summary>
    public class MageTest : CharacterTests 
    {
        
        /// <summary>
        /// Assert primary attributes are set correctly. 
        /// </summary>
        [Fact]
        public void MageConstructor_ConstructorPrimaryAttributes_ShouldBeDefault()
        {

        
            Mage mage =new("name");

            PrimaryAttribute expected = new() {
                Vitality = 5,
                Strength = 1,
                Dexterity = 1,
                Intelligence = 8
                };


            PrimaryAttribute actual = mage.BasePrimaryAttribute;

            Assert.True(actual.Equals(expected));

        }

        /// <summary>
        /// Checks that attributes change as expected when leveling up.
        /// </summary>
        [Fact]
        public void LevelUp_MageAttributes_HasIncreasedAsExpected()
        {
            Mage mage = new("name");

            PrimaryAttribute expected = new()
            {
                Vitality = 8,
                Strength = 2,
                Dexterity = 2,
                Intelligence = 13
            };

            mage.LevelUp();

            PrimaryAttribute actual = mage.BasePrimaryAttribute;
            Assert.True(actual.Equals(expected));

        }



        }


    /// <summary>
    /// Testing rogue character
    /// </summary>
    public class RogueTest : CharacterTests
    {
        

        /// <summary>
        /// Default attributes are correct
        /// </summary>
        [Fact]
        public void RogueConstructor_ConstructorPrimaryAttributes_ShouldBeDefault()
        {


            Rogue rogue = new("name");

            PrimaryAttribute expected = new()
            {
                Vitality = 8,
                Strength = 2,
                Dexterity = 6,
                Intelligence = 1
            };


            PrimaryAttribute actual = rogue.BasePrimaryAttribute;

            Assert.True(actual.Equals(expected));

        }

        /// <summary>
        /// Leveling up correctly
        /// </summary>
        [Fact]
        public void LevelUp_RogueAttributes_HasIncreasedAsExpected()
        {
           Rogue rogue = new("name");

            PrimaryAttribute expected = new()
            {
                Vitality = 11,
                Strength = 3,
                Dexterity = 10,
                Intelligence = 2
            };

            rogue.LevelUp();

            PrimaryAttribute actual = rogue.BasePrimaryAttribute;
            Assert.True(actual.Equals(expected));

        }
    }

    /// <summary>
    /// Warriortest
    /// </summary>
    public class WarriorTest : CharacterTests // ??? 
    {

        /// <summary>
        /// Primary attrs correct.
        /// </summary>
        [Fact]
        public void WarriorConstructor_ConstructorPrimaryAttributes_ShouldBeDefault()
        {


            Warrior warrior = new("name");

            PrimaryAttribute expected = new()
            {
                Vitality = 10,
                Strength = 5,
                Dexterity = 2,
                Intelligence = 1
            };


            PrimaryAttribute actual = warrior.BasePrimaryAttribute;

            Assert.True(actual.Equals(expected));

        }

        /// <summary>
        /// Attrs increment  correctly
        /// </summary>
        [Fact]
        public void LevelUp_WarriorAttributes_HasIncreasedAsExpected()
        {
            Warrior warrior = new("name");

            PrimaryAttribute expected = new()
            {
                Vitality = 15,
                Strength = 8,
                Dexterity = 4,
                Intelligence = 2
            };

            warrior.LevelUp();

            PrimaryAttribute actual = warrior.BasePrimaryAttribute;
            Assert.True(actual.Equals(expected));
        }

        /// <summary>
        /// Secondary attrs with level up
        /// </summary>
        [Fact]
        public void LevelUp_WarriorSecondaryAttributes_IsAsExpected ()
        {

            // Arrange
            Warrior warrior = new("name");
         
            SecondaryAttribute expected = new() { Health = 150, ArmorRating = 12, ElementalResistance = 2};

            
            // Act
            warrior.LevelUp();

            SecondaryAttribute actual = warrior.SecondaryAttribute;
            // Assert

            Assert.Equal(expected, actual);
        }  

    }

    /// <summary>
    /// Rangertest
    /// </summary>
    public class RangerTest : CharacterTests 
    {

        /// <summary>
        /// Constructed with correct attrs
        /// </summary>
        [Fact]
        public void RangerConstructor_ConstructorPrimaryAttributes_ShouldBeDefault()
        {


            Ranger ranger= new("name");

            PrimaryAttribute expected = new()
            {
                Vitality = 8,
                Strength = 1,
                Dexterity = 7,
                Intelligence = 1
            };


            PrimaryAttribute actual = ranger.BasePrimaryAttribute;

            Assert.True(actual.Equals(expected));

        }

        /// <summary>
        /// Levelup to correct attributes
        /// </summary>
        [Fact]
        public void LevelUp_RangerAttributes_HasIncreasedAsExpected()
        {
            Ranger ranger = new("name");

            PrimaryAttribute expected = new()
            {
                Vitality = 10,
                Strength = 2,
                Dexterity = 12,
                Intelligence = 2
            };

            ranger.LevelUp();

            PrimaryAttribute actual = ranger.BasePrimaryAttribute;
            Assert.True(actual.Equals(expected));
        }
    }
}
