﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using RPGcharacters.Characters;

namespace RPGcharactersTest.CharactersTest
{
    public class CharacterAttributeTest
    {
        /// <summary>
        /// Testing that a character is created with level = 1.
        /// </summary>
        [Fact]
        public void CharacterConstructor_CharacterLevel_ShouldBeOne()
        {
            int expected = 1;
            Character character = new Mage("name");

            int actual = character.Level;

            Assert.Equal(expected, actual);
            
        }

        /// <summary>
        /// Checks that LevelUp() increases the level by one.
        /// </summary>
        [Fact]
        public void LevelUp_Level_ShouldBeTwo()
        {
            int expected = 2;
            Character character = new Mage("name");
            
            
            character.LevelUp();
            int actual = character.Level;

            Assert.Equal(expected, actual);
            
        }

        /// <summary>
        /// Checks that leveling up multiple times throws exception for negative levels. 
        /// </summary>
        /// <param name="nlevels"></param>
        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void LevelUp_LevelUpNullOrNegative_ThrowsException(int nlevels) 
        {
            Character character = new Mage("name");

            Assert.Throws<ArgumentException>(() => character.LevelUp(nlevels));

        }

        /// <summary>
        /// Asserts correct attributes is set as a character is created. 
        /// </summary>
        [Fact]
        public void MageConstructor_DefaultAttributesAtStart_IsAsExpected()
        {
            Mage mage = new("name");


            PrimaryAttribute expected = new()
            {
                Vitality = 5,
                Strength = 1,
                Dexterity = 1,
                Intelligence = 8
            };

            Assert.Equal(mage.BasePrimaryAttribute, expected);
    }
    }

   
}
